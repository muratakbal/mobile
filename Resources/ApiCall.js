/**
 * @author Murat AKBAL
 * 
 * @example  
 * 	//authorized call
 * 	var call = new IdinApi();
 * 	call.onload = function(e) {
 * 		json = JSON.parse(e.text);
 * 	}
 * 	call.callWithToken('user/');
 * 
 */
Ti.include("sha1.js");
Ti.include('jsOAuth-1.3.6.min.js');

var RequestUtils = {} 
RequestUtils.SECRET_KEY = 'rand0m$trinGolusturmak1yid1r';
RequestUtils.SEPEARTOR = '*_*';
RequestUtils.convetToGetParams = function(requestObject) {
	var retVal = '';
	for(index in requestObject) {							
		retVal += index + '=' + requestObject[index] + '&';
	}
	
	return retVal;
}
RequestUtils.sortReq = function(o) {
						    var sorted = {},
						    key, a = [];
						
						    for (key in o) {
						        if (o.hasOwnProperty(key)) {
						                a.push(key);
						        }
						    }
						
						    a.sort();
						
						    for (key = 0; key < a.length; key++) {
						        sorted[a[key]] = o[a[key]];
						    }
						    return sorted;
						};
RequestUtils.hash = function(requestObject) {
						var retVal = '';
						for(index in requestObject) {							
							retVal += index + RequestUtils.SEPEARTOR + requestObject[index];
						}
						retVal += ':' + RequestUtils.SECRET_KEY;

						return SHA1(retVal);
					};

// declare ApiCall
function ApiCall() {}

ApiCall.prototype.baseUrl = 'http://127.0.0.1:8000/';
ApiCall.prototype.consumerKey = 'asdasd';
ApiCall.prototype.consumerSecret = 'asdasd';

ApiCall.prototype.onload = function(e) {
	json = JSON.parse(e.text);
	Ti.API.info(json);
};

ApiCall.prototype.onerror = function(e) {
	json = JSON.parse(e.text);
	Ti.API.info('ERROR:');
	Ti.API.info(json);
};

/**
 * @param {Object} requestObject
 */
ApiCall.prototype.callWithToken = function(method) {
	var oauth = OAuth({
					consumerKey: this.consumerKey,
    				consumerSecret: this.consumerSecret,
					accessTokenKey: Ti.App.Properties.getString('accessToken'),
					accessTokenSecret:	Ti.App.Properties.getString('accessTokenSecret')
				});

    oauth.get(this.baseUrl + 'api/v1/' + method, this.onload, this.onerror);
}

/**
 * @param {Object} requestObject
 */
ApiCall.prototype.loginCall = function(requestObject) {
	requestObject['x_auth_mode'] = 'client_auth';
    requestObject = RequestUtils.sortReq(requestObject);  
    requestObject['idinx'] = RequestUtils.hash(requestObject);
    
    var oauth = OAuth({
    				consumerKey: this.consumerKey,
    				consumerSecret: this.consumerSecret
    			});
    
    oauth.get(this.baseUrl + 'account/login?' + RequestUtils.convetToGetParams(requestObject), this.onload, this.onerror);
}

module.exports = ApiCall;