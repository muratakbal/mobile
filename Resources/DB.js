/**
 * 
 * usage Database
 * var Database = require('DB');
 * var db = new Database('test');
 * db.initialize();
 * db.query("insert into heyo(id) values('1200')");
 * db.closeConnection();
 * 
 * var results = db.query("SELECT * FROM heyo");
 * 	while(results.isValidRow()) {
 * 		console.log(results.getFieldByName('id'));
 * 		results.next();
 * 	}
 * db.closeConnection();
 * 
 * 
 * @author Murat AKBAL
 */

function DB(dbName) {
	var databaseName = dbName;
	var db;
	
	this.initialize = function() {
		db = Ti.Database.open(databaseName);
		db.file.setRemoteBackup(false);
		db.execute('DROP TABLE IF EXISTS heyo;');
		db.execute('CREATE TABLE IF NOT EXISTS heyo("id" text(100,0));');
		db.close();
	};
	
	this.query = function(sql) {		
		db = Ti.Database.open(databaseName);
		return db.execute(sql);
	};
	
	this.closeConnection = function() {
		db.close();
	}
};

//make constructor function the public component interface
module.exports = DB;