/**
 * handle all events related facebok connect
 * @param string appId #facebook appId
 * @param array permissions #facebook permission 
 */
function FacebookConnect(appId, permissions) {
	/**
	 * Facebook App Id
	 */
	Ti.Facebook.appid = appId; //423698161001597
	
	/**
	 * Facebook Permissions
	 * check ref: http://developers.facebook.com/docs/authentication/permissions/
	 */
	Ti.Facebook.permissions = permissions; //['publish_stream'];
	
	/**
	 * add login event handler
	 */
	this.addLoginListener = function() {
		Ti.Facebook.addEventListener('login', function(event) {
			if(event.success) {
				var apiCall = new IdinApi();
				apiCall.onload = function(e) {
					json = JSON.parse(e.text);
					Ti.API.info(json);
					
					if(!json.success) {
						Ti.Facebook.logout();
					} else {
						Ti.App.Properties.setString('accessToken', json.access_token);
						Ti.App.Properties.setString('accessTokenSecret', json.access_token_secret);
					}
				}
				
				apiCall.loginCall({
					'x_auth_password': Ti.Facebook.accessToken,
					'x_auth_username': Ti.Facebook.uid
				});
				console.log('Logged In', Ti.Facebook.uid, Ti.Facebook.accessToken);
			} else if(event.cancelled) {
				console.log('User Cancelled');
			} else if(event.error) {
				console.log('An error occured: ' + event.error);
			}			
		});
	};
	
	this.removeLoginListener = function() {
		Ti.App.removeEventListener('login');
	};
	
	/**
	 * add logout event listener
	 */
	this.addLogoutListener = function() {
		Ti.Facebook.addEventListener('logout', function(event) {
			console.log('Logged Out');
		});	
	};
	
	/**
	 * remove logout event listener
	 */
	this.removeLogoutListener = function() {
		Ti.App.removeEventListener('logout');
	};
	
	/**
	 * add facebookConnect event listener
	 * when event is fired, execute related function
	 */
	this.addConnectListener = function() {
		Ti.App.addEventListener('facebookConnect', function() {
			if(!Ti.Facebook.loggedIn) {
				Ti.Facebook.authorize();
			} else {
				Ti.Facebook.logout();
			}
		});
	};
	
	/**
	 * remove facebookConnect event listener
	 */
	this.removeConnectListener = function() {
		Ti.App.removeEventListener('facebookConnect');
	}
};

module.exports = FacebookConnect;