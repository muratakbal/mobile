/**
* Appcelerator Titanium Mobile
* This is generated code. Do not modify. Your changes *will* be lost.
* Generated code is Copyright (c) 2009-2011 by Appcelerator, Inc.
* All Rights Reserved.
*/
#import <Foundation/Foundation.h>
#import "TiUtils.h"
#import "ApplicationDefaults.h"
 
@implementation ApplicationDefaults
  
+ (NSMutableDictionary*) copyDefaults
{
    NSMutableDictionary * _property = [[NSMutableDictionary alloc] init];

    [_property setObject:[TiUtils stringValue:@"l9gFlOL2gFam3lOJD0aPyJ7nxkpviOQZ"] forKey:@"acs-oauth-secret-development"];
    [_property setObject:[TiUtils stringValue:@"uvWWwaW34JiNZAvRzeSjEAGiILkulJwT"] forKey:@"acs-oauth-key-development"];
    [_property setObject:[TiUtils stringValue:@"4UgHcdhlt1PytuctshTRIpb2WD3oGGT2"] forKey:@"acs-api-key-development"];
    [_property setObject:[TiUtils stringValue:@"2okss3pSYMSnblz1ucoKRVIGRI1kNGfO"] forKey:@"acs-oauth-secret-production"];
    [_property setObject:[TiUtils stringValue:@"Kex6V82icL9ZKlUQantYdGRdI8NJ2Gvh"] forKey:@"acs-oauth-key-production"];
    [_property setObject:[TiUtils stringValue:@"BKfBxouDkHqe4lKLuOywAwupYPUsHu6s"] forKey:@"acs-api-key-production"];
    [_property setObject:[TiUtils stringValue:@"423698161001597"] forKey:@"ti.facebook.appid"];

    return _property;
}
@end
